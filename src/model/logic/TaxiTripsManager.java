package model.logic;


import api.ITaxiTripsManager;
import model.data_structures.*;
import model.vo.*;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	private HTSeparateChaining<String,List<String,Service>> servicios;
	private HTSeparateChaining<Double,List<Double,Service>> serviciosPorMillas;

	public void loadServices(String direccionJson) {
		servicios = new HTSeparateChaining<String,List<String,Service>>(1);
		serviciosPorMillas = new HTSeparateChaining<Double, List<Double,Service>>(1);
		List<String,Service> listaServicios = new List<String, Service>();
		List<Double,Service> listaPorMillas = new List<Double, Service>();
		System.out.println("Cargando datos de  " + direccionJson);
		System.out.println("\n-------------------------------------------------------");
		JsonParser parser = new JsonParser();
		try{
			JsonArray arr = (JsonArray) parser.parse(new FileReader(direccionJson));
			for (int i = 0; arr != null && i < arr.size(); i++){
				JsonObject obj= (JsonObject) arr.get(i);

				String id = "Unknown";
				if ( obj.get("taxi_id") != null ){
					id = obj.get("taxi_id").getAsString();}

				String comp = "Independent Owner";
				if ( obj.get("company") != null ){ 
					comp = obj.get("company").getAsString();}

				String dca = "Unknown";
				if ( obj.get("dropoff_community_area") != null ){
					dca = obj.get("dropoff_community_area").getAsString(); }

				String pca = "Unknown";
				if ( obj.get("pickup_community_area") != null ){
					pca = obj.get("pickup_community_area").getAsString(); }

				String tripid = "Unknown";
				if ( obj.get("trip_id") != null ){
					tripid = obj.get("trip_id").getAsString(); }

				String tripst = "Unknown";
				if ( obj.get("trip_start_timestamp") != null ){
					tripst = obj.get("trip_start_timestamp").getAsString(); }

				String tripet = "Unknown";
				if ( obj.get("trip_end_timestamp") != null ){
					tripet = obj.get("trip_end_timestamp").getAsString(); }

				int tripseconds = -1;
				if ( obj.get("trip_seconds") != null ){
					tripseconds = obj.get("trip_seconds").getAsInt(); }

				double tripmiles = -1;
				if ( obj.get("trip_miles") != null ){
					tripmiles = obj.get("trip_miles").getAsDouble(); }

				double triptotal = -1;
				if ( obj.get("trip_total") != null ){ 
					triptotal = obj.get("trip_total").getAsDouble(); }

				//Carga de servicios en lista.
				Service nuevo = new Service(id, comp, dca, pca, tripid, tripst, tripet, tripseconds, tripmiles, triptotal);
				listaServicios.agregarEnOrden(pca, nuevo);
				listaPorMillas.agregarEnOrden(tripmiles,nuevo);
			}
			for(int tempPCA = 0; tempPCA <=80 ; tempPCA++){
				List<String,Service> serviciosPCA = new List<String, Service>();
				Nodo<String,Service> actual = listaServicios.darPrimero();
				while(actual!=null){
					if(!actual.getValue().getPickupCommunityArea().equals("Unknown")){
						if(Integer.parseInt(actual.getValue().getPickupCommunityArea())==tempPCA){
							serviciosPCA.agregarEnOrden(actual.getKey(), actual.getValue());
						}
					}
					actual = actual.darSiguiente();
				}
				if(!serviciosPCA.isEmpty()){
					servicios.put(serviciosPCA.darPrimero().getKey(),serviciosPCA);
				}
			}

			for(int tempMilla = 0; tempMilla <=80 ; tempMilla++){
				List<Double,Service> serviciosM = new List<Double, Service>();
				Nodo<Double,Service> actualM = listaPorMillas.darPrimero();
				while(actualM!=null){
					if(actualM.getValue().getTripMiles() != -1){
						if(actualM.getValue().getTripMiles() == tempMilla){
							serviciosM.agregarEnOrden(actualM.getKey(), actualM.getValue());
						}
					}
					actualM = actualM.darSiguiente();
				}
				if(!serviciosM.isEmpty()){
					serviciosPorMillas.put(serviciosM.darPrimero().getKey(),serviciosM);
				}
			}
			System.out.println("\nSe cargaron "+servicios.getTamano()+" grupos de servicios por area de comunidad.");
		}
		catch (JsonIOException e1 ) {
			e1.printStackTrace();}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();}
		catch (FileNotFoundException e3) {
			e3.printStackTrace();}
	}

	public List<String, Service> darServiciosPCA(String pPCA){
		boolean agrego = false;
		List<String,Service> listaSPCA = servicios.get(pPCA);
		List<String,Service> listaOrdenada = new List<String, Service>();
		if(listaSPCA != null){
			Nodo<String,Service> actual = listaSPCA.darPrimero();
			while(actual != null){
				if(listaOrdenada.darPrimero() == null){
					listaOrdenada.cambiarPrimero(actual);
					listaOrdenada.calcularLongitud();
					agrego = true;
				}
				else if(listaOrdenada.darPrimero().getValue().compareTime(actual.getValue())>0){
					actual.cambiarSiguiente(actual.darSiguiente());
					listaOrdenada.cambiarPrimero(actual);
					listaOrdenada.calcularLongitud();
					agrego = true;
				}
				else
				{
					Nodo<String,Service> actual2 = listaOrdenada.darPrimero();
					while(actual2.darSiguiente()!=null&&!agrego){
						if(actual2.darSiguiente().getValue().compareTime(actual.getValue())>0){
							actual.cambiarSiguiente(actual2.darSiguiente());
							actual2.cambiarSiguiente(actual);
							listaOrdenada.calcularLongitud();
							agrego = true;
						}
						actual2 = actual2.darSiguiente();
					}
					if(!agrego){
						actual2.cambiarSiguiente(actual);
						listaOrdenada.calcularLongitud();
						agrego = true;
					}
				}
				actual = actual.darSiguiente();
			}
		}
		return listaOrdenada;
	}


	public List<Double, Service> darServiciosMillas(Double pMillas){
		List<Double,Service> listaM = serviciosPorMillas.get(pMillas);
		return listaM;
	}
}