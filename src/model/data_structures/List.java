package model.data_structures;

public class List<Key extends Comparable<Key>,Value extends Comparable<Value>> implements LinkedList<Key,Value>, Comparable<List<Key,Value>> {

	/**
	 * Primer nodo de la lista 
	 */
	private Nodo<Key, Value> primero;

	/**
	 * Longitud de la lista.
	 */
	private int longitud;

	/**
	 * Crea una nueva lista sin elementos.
	 */
	public List(){
		primero = null;
		longitud = 0;
	}

	/**
	 * Retorna el primer nodo de la lista.
	 * @return primer nodo de la lista.
	 */
	public Nodo<Key,Value> darPrimero(){
		return primero;
	}
	/**
	 * Retorna la longitud de la lista.
	 * @return Longitud de la lista.
	 */
	public int darLongitud()
	{
		return longitud;
	}

	/**
	 * Agrega un nuevo nodo a la lista con los elementos dados por parámetro.
	 */
	public void agregar(Key key, Value value)
	{
		Nodo<Key,Value> nuevo = new Nodo<Key,Value>(key,value);
		if(primero == null)
		{
			primero = nuevo;
			longitud++;
		}
		else if(buscar(key)==null)
		{
			nuevo.cambiarSiguiente(primero);
			primero = nuevo;
			longitud++;
		}
		else{
			buscarNodo(key).changeValue(value);
		}
	}
	

	//Compara y agrega en orden por valor.
	public boolean agregarEnOrden(Key key, Value value){
		boolean agrego = false;
		Nodo<Key,Value> nuevo = new Nodo<Key,Value>(key,value);
		if(primero == null)
		{
			primero = nuevo;
			longitud++;
			agrego = true;
		}
		else if(primero.getValue().compareTo(value)>0){
			nuevo.cambiarSiguiente(primero);
			primero = nuevo;
			longitud++;
			agrego = true;
		}
		else{
			Nodo<Key,Value> actual = this.darPrimero();
			while(actual.darSiguiente()!=null&&!agrego){
				if(actual.darSiguiente().getValue().compareTo(value)>0){
					nuevo.cambiarSiguiente(actual.darSiguiente());
					actual.cambiarSiguiente(nuevo);
					longitud++;
					agrego = true;
				}
				actual = actual.darSiguiente();
			}
			if(!agrego){
				actual.cambiarSiguiente(nuevo);
				longitud ++;
				agrego = true;
			}
		}
		return agrego;
	}
	public void cambiarPrimero(Nodo<Key,Value> pPrimero) {
		primero=pPrimero;
	}

	/**
	 * Elimina el nodo con la llave dada por parámetro.
	 */
	public Value eliminar(Key pKey)
	{
		Value eliminado = null;
		boolean elim = false;
		if(primero.getKey().compareTo(pKey)==0){
			eliminado = primero.getValue();
			primero = primero.darSiguiente();
			longitud--;
			elim = true;
		}
		else{
			Nodo<Key,Value> nodoActual = primero;
			while(nodoActual.darSiguiente() != null && !elim){
				if(nodoActual.darSiguiente().getKey().compareTo(pKey)==0){
					eliminado = nodoActual.darSiguiente().getValue();
					nodoActual.cambiarSiguiente(nodoActual.darSiguiente().darSiguiente());
					longitud--;
					elim = true;
				}
			}
			if(!elim && nodoActual.getKey().compareTo(pKey)==0)
			{
				eliminado = nodoActual.getValue();
				nodoActual = null;
				longitud--;
				elim = true;
			}
		}
		return eliminado;
	}

	public boolean isEmpty(){
		return longitud==0;
	}

	/**
	 * Busca el valor que contiene la llave dada por parametro. 
	 * @param key llave del elemento buscado.
	 * @return Valor que corresponde a la llave. Null si no lo encuentra.
	 */
	public Value buscar(Key pKey){
		Nodo<Key,Value> actual = this.darPrimero();
		while(actual!=null){
			if(actual.getKey().compareTo(pKey)==0){
				return actual.getValue();
			}
			actual = actual.darSiguiente();
		}
		return null;
	}
	
	/**
	 * Busca el valor que contiene la llave dada por parametro. 
	 * @param key llave del elemento buscado.
	 * @return Valor que corresponde a la llave. Null si no lo encuentra.
	 */
	public Nodo<Key,Value> buscarNodo(Key pKey){
		Nodo<Key,Value> actual = this.darPrimero();
		while(actual!=null){
			if(actual.getKey().compareTo(pKey)==0){
				return actual;
			}
			actual = actual.darSiguiente();
		}
		return null;
	}
	
	public int compareTo(List<Key,Value> pList){
		int rta = 0;
		if(this.darLongitud()>pList.darLongitud()){
			rta=1;
		}
		else if(this.darLongitud()<pList.darLongitud()){
			rta=-1;
		}
		return rta;
	}
	
	public void calcularLongitud(){
		int lon = 0;
		Nodo<Key,Value> actual = this.darPrimero();
		while(actual != null){
			lon++;
			actual = actual.darSiguiente();
		}
		longitud = lon;
	}
}