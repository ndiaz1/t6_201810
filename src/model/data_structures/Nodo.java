package model.data_structures;

public class Nodo<Key extends Comparable<Key>, Value> {

	/**
	 * Nodo siguiente del nodo actual.
	 */
	private Nodo<Key,Value> siguiente;

	/**
	 * Llave del nodo actual.
	 */
	private Key llave;


	/**
	 * Contenido del nodo actual.
	 */
	private Value valor;


	/**
	 * Crea un nuevo nodo con el elemento dado por parametro.
	 */
	public Nodo (Key k, Value v){
		llave = k;
		valor = v;
		siguiente = null;
	}

	/**
	 * Retorna la llave del nodo.
	 * @return llave del nodo.
	 */
	public Key getKey(){
		return llave;
	}

	/**
	 * Retorna el elemento que est� dentro del nodo.
	 * @return elemento contenido dentro del nodo.
	 */
	public Value getValue(){
		return valor;
	}
	
	/**
	 * Cambia la llave del nodo.
	 */
	public void changeKey(Key pLlave){
		llave = pLlave;
	}
	
	/**
	 * Cambia el elemento que est� dentro del nodo.
	 */
	public void changeValue(Value pValor){
		valor = pValor;
	}

	/**
	 * Retorna el siguiente nodo del nodo actual.
	 * @return Siguiente nodo.
	 */
	public Nodo<Key, Value> darSiguiente(){
		return siguiente;
	}

	/**
	 * Cambia el nodo siguiente del nodo actual. 
	 * @param nuevo Nodo a asignar como siguiente.
	 */
	public void cambiarSiguiente(Nodo<Key, Value> nuevo){
		siguiente = nuevo;
	}
}