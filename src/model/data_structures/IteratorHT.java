package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class IteratorHT<Key extends Comparable<Key>,Value>implements Iterator<Key>{

	private Nodo<Key,Value> nodoProximo;
	
	IteratorHT(Nodo<Key,Value> pNodo){
		nodoProximo = pNodo;
	}
	public boolean hasNext() {
		return nodoProximo != null;
	}

	public Key next() {
		if(nodoProximo == null){
			throw new NoSuchElementException("No hay proximo.");
		}
		Key llave = nodoProximo.getKey();
		nodoProximo = nodoProximo.darSiguiente();
		return llave;
	}
}
