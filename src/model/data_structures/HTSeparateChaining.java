package model.data_structures;

//Adaptado de http://homepage.divms.uiowa.edu/~kvaradar/sp2009/SeparateChainingHashTable.java

public class HTSeparateChaining<Key extends Comparable <Key>, Value extends Comparable<Value>> {

	private int capacidad;
	private int tamanoActual;
	private List<Key,Value>[] areaPrimaria;

	public HTSeparateChaining(int pCapacidad){
		areaPrimaria = new List[nextPrime(pCapacidad)];
		for(int i = 0; i<areaPrimaria.length;i++){
			areaPrimaria[i]=new List<Key,Value>();
		}
		capacidad = pCapacidad;
		tamanoActual = 0;
	}
	
	public void calcularTamano(){
		int tamanoNuevo = 0;
		for(int i = 0; i<areaPrimaria.length;i++){
			tamanoNuevo += areaPrimaria[i].darLongitud();
		}
		tamanoActual = tamanoNuevo;
	}
	
	public int getTamano(){
		calcularTamano();
		return tamanoActual;
	}
	
	public int getCapacidad(){
		return capacidad;
	}
	

	/**
	 * Agrega una nueva dupla en el arreglo. Si el valor ingresado es null, no altera el arreglo y no agrega la dupla.
	 * @param key Llave a ingresar
	 * @param value Valor de la dupla
	 */
	public void put(Key key, Value value){
		if(value!=null){
			int posicion = hash(key);
			areaPrimaria[posicion].agregar(key, value);
			rehash();
		}
	}

	public void putNoReplace(Key key, Value value){
		if(value!=null){
			int posicion = hash(key);
			areaPrimaria[posicion].agregarEnOrden(key, value);
			rehash();
		}
	}
	/**
	 * @param key Llave de la dupla.
	 * @return Valor correspondiente a la llave. Null si la llave no existe.
	 */
	public Value get(Key key){
		Value buscado = areaPrimaria[hash(key)].buscar(key);
		return buscado;
	}
	/**
	 * Elimina el nodo de acuerdo a la llave dada por parámetro.
	 * @param key Llave del nodo a eliminar.
	 * @return Valor eliminado.
	 */
	public Value delete(Key key){
		int posicion = hash(key);
		Value eliminado = areaPrimaria[posicion].eliminar(key);
		return eliminado;
	}

	public void rehash(){
		if((this.getTamano()/capacidad)>6){
			List<Key,Value>[] listaAntigua = areaPrimaria;
			areaPrimaria = new List[nextPrime(capacidad*2)];
			for(int i = 0; i<areaPrimaria.length;i++){
				areaPrimaria[i]=new List<Key,Value>();
			}
			tamanoActual = 0;
			capacidad = 2*capacidad;
			for( int i = 0; i < listaAntigua.length; i++ ){
				Nodo<Key, Value> actual = listaAntigua[i].darPrimero();
				while(actual != null){
					Key kActual = actual.getKey();
					Value vActual = actual.getValue();
					put(kActual,vActual);
					actual = actual.darSiguiente();
				}
			}
		}
	}

	private int hash(Key key){
		return (key.hashCode() & 0x7fffffff) % capacidad;
	}

	/**
	 * Internal method to find a prime number at least as large as n.
	 * @param n the starting number (must be positive).
	 * @return a prime number larger than or equal to n.
	 */
	private static int nextPrime( int n )
	{
		if( n % 2 == 0 )
			n++;

		for( ; !isPrime( n ); n += 2 )
			;

		return n;
	}

	/**
	 * Internal method to test if a number is prime.
	 * Not an efficient algorithm.
	 * @param n the number to test.
	 * @return the result of the test.
	 */
	private static boolean isPrime( int n )
	{
		if( n == 2 || n == 3 )
			return true;

		if( n == 1 || n % 2 == 0 )
			return false;

		for( int i = 3; i * i <= n; i += 2 )
			if( n % i == 0 )
				return false;

		return true;
	}
}
