package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service>{
	private String taxi_id;
	private String company;
	private String dropoff_community_area;
	private String pickup_community_area;
	private String trip_id;
	private int trip_seconds;
	private double trip_miles;
	private double trip_total;
	private String trip_start_timestamp;
	private String trip_end_timestamp;

	public Service(String id, String comp, String dca, String pca, String tripid,String tripstarttime, String tripendtime, int tripseconds, double tripmiles, double triptotal){
		taxi_id=id;
		company=comp;
		dropoff_community_area=dca;
		pickup_community_area=pca;
		trip_id=tripid;
		trip_seconds=tripseconds;
		trip_miles=tripmiles;
		trip_total=triptotal;
		trip_start_timestamp=tripstarttime;
		trip_end_timestamp=tripendtime;
	}

	/**
	 * @return company - Compañía a la que pertenece el servicio.
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @return Community area - Zona en la que finaliza el recorrido.
	 */
	public String getCommunityArea(){
		return dropoff_community_area;
	}

	/**
	 * @return Community area - Zona en la que inicia el recorrido.
	 */
	public String getPickupCommunityArea(){
		return pickup_community_area;
	}

	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return trip_id;
	}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxi_id;
	}

	/**
	 * @return trip_start_timestamp
	 */
	public String getStartTimeC() {
		String tiempo = trip_start_timestamp;
		return tiempo;
	}

	public String[] getStartTime() {
		String[] tiempo = trip_start_timestamp.split("T");
		return tiempo;
	}

	/**
	 * @return trip_end_timestamp
	 */
	public String getEndTimeC() {
		String tiempo = trip_end_timestamp;
		return tiempo;
	}
	public String[] getEndTime() {
		String[] tiempo = trip_end_timestamp.split("T");
		return tiempo;
	}

	/**
	 * @return time - Tiempo de viaje en segundos.
	 */
	public int getTripSeconds() {
		return trip_seconds;
	}

	/**
	 * @return miles - Distancia del viaje en millas.
	 */
	public double getTripMiles() {
		return trip_miles;
	}

	/**
	 * @return total - Costo total del viaje
	 */
	public double getTripTotal() {
		return trip_total;
	}

	/**
	 * Compara por el PickupCommunityArea.
	 */
	public int compareTo(Service o) {
		int x=0;
		if(this.getPickupCommunityArea().compareTo(o.getPickupCommunityArea())>0)
		{
			x = 1;
		}
		else if(this.getPickupCommunityArea().compareTo(o.getPickupCommunityArea())<0)
		{
			x = -1;
		}
		return x;
	}
	public int compareTime(Service o) {
		int x=0;
		String a =this.getStartTimeC();
		String b =o.getStartTimeC();
		if(a.compareTo(b)>0)
		{
			x = 1;
		}
		else if(a.compareTo(b)<0)
		{
			x = -1;
		}
		else if(a.compareTo(b)==0)
		{
			x = 0;
		}
		return x;
	}
	public int compareDistance(Service o) {
		int x=0;
		double a =this.getTripMiles();
		double b =o.getTripMiles();
		if(a>(b))
		{
			x = 1;
		}
		else if(a<(b))
		{
			x = -1;
		}
		else if(a==(b))
		{
			x = 0;
		}
		return x;
	}
	/**
	 * Representación del objeto como String.
	 */
	public String toString()
	{
		return taxi_id+"----"+company+"----"+pickup_community_area;
	}
}