package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.*;
import model.logic.TaxiTripsManager;
import model.vo.*;

/**
 * view del programa
 */
public class TaxiTripsManagerView {

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//cargar informacion dada
			case 1:

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;

					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.loadServices(linkJson);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB\n");

				break;
			case 2:
				//Punto 1
				System.out.println("Ingrese el area de comunidad de inicio de los servicios:");
				String pPCA = sc.next();
				List<String,Service> serviciosPCA = Controller.darServiciosPCA(pPCA);
				if(!serviciosPCA.isEmpty()){
					Nodo<String,Service> actual = serviciosPCA.darPrimero();
					while(actual != null){
						System.out.println(actual.getKey()+" - "+actual.getValue().getStartTimeC()+" - "+actual.getValue().getTripId()+" - "+actual.getValue().getEndTimeC());
						actual = actual.darSiguiente();
					}
					System.out.println("PCA      Tiempo de Inicio                        ID                          Tiempo de Llegada");
				}
				else
				{
					System.out.println("No se encontraron servicios con el area de comunidad especificada.");
				}
				break;
			case 3:
				//Punto 2
				System.out.println("Ingrese las millas recorridas:");
				String pStringMillas = sc.next();
				Double pMillas = Double.parseDouble(pStringMillas);
				if(pMillas%1 != 0){
					pMillas = (double) Math.round(pMillas);
				}
				List<Double,Service> serviciosM = Controller.darServiciosMillas(pMillas);
				if(!serviciosM.isEmpty()){
					Nodo<Double,Service> actualM = serviciosM.darPrimero();
					while(actualM != null){
						System.out.println(actualM.getValue().getTripId()+" - "+actualM.getValue().getTripMiles());
						actualM = actualM.darSiguiente();
					}
					System.out.println("     ID                       Millas recorridas");
				}
				else
				{
					System.out.println("No se encontraron servicios con las millas especificadas.");
				}
				break;
			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("\n---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------\n");
		System.out.println("1. Cargar datos.");
		System.out.println("2. Punto 1 - Dada un area, retorna los servicios que inician alli, ordenados cronol�gicamente.");
		System.out.println("3. Punto 2 - Define los grupos de servicios por millas.");
		System.out.println("\nType the option number for the task, then press enter: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- �Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}

}
