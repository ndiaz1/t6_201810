package controller;

import model.data_structures.List;
import model.logic.TaxiTripsManager;
import model.vo.*;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static TaxiTripsManager  manager = new TaxiTripsManager();

	/** To load the services of the taxi with taxiId */
	public static void loadServices( String serviceFile) {
		manager.loadServices(serviceFile);
	}
	
	public static List<String, Service> darServiciosPCA(String pPCA){
		return manager.darServiciosPCA(pPCA);
	}
	
	public static List<Double,Service> darServiciosMillas(Double pMillas){
		return manager.darServiciosMillas(pMillas);
	}
}
