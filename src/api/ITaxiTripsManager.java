package api;


/**
 * Basic API for testing the functionality of the TaxiTrip manager
 */
public interface ITaxiTripsManager {

	/** 
	 * @param servicesFile - path to the JSON file with taxi services 
	 */
	public void loadServices(String serviceFile);
}
